package ai.deeplens;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import com.opencsv.CSVReader;

public class TrialReaderCsvImpl implements TrialReader {
    private String _fileName;

    public TrialReaderCsvImpl(String fileName) {
        _fileName = fileName;
    }

    @Override
    public Trial read() throws IOException {
        CSVReader reader = new CSVReader(new FileReader(_fileName));
        List<String[]> rows = reader.readAll();
//        System.out.println("# rows = " + rows.size());
        if (null != rows && 1 < rows.size()) {
//            for (String[] row : rows) {
//                for (String cell : row) {
//                    System.out.print(cell + " ");
//                }
//                System.out.println();
//            }
            return(new TrialImpl(rows.get(1)));
        }
        throw new IOException("No data in file " + _fileName);
    }
}
