package ai.deeplens;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PatientReaderCsvImpl implements PatientReader {
    private String _fileName;

    public PatientReaderCsvImpl(String fileName) {
        _fileName = fileName;
    }

    public List<Patient> read() throws IOException {
        CSVReader reader = new CSVReader(new FileReader(_fileName));
        List<String[]> rows = reader.readAll();

        if (null != rows && 1 < rows.size()) {
            rows = rows.subList(1, rows.size()); // Skip the column headers row
            List<Patient> patients = new ArrayList<Patient>();
            for (String[] row : rows) {
                PatientImpl p = new PatientImpl(row);
                //System.out.println(p.toString());
                patients.add(p);
            }
            return patients;
        }
        throw new IOException("No data in file " + _fileName);
    }
}
