package ai.deeplens;

import java.util.List;

/**
 * Assumes there may be multiple implementations, each with different rules.
 * Maintaining multiple implementations is simpler and more reliable than
 * morphing one implementation over time.
 */
public interface Matcher {
    public boolean match(Trial trial, Patient patient);
}
