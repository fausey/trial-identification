package ai.deeplens;

import java.util.ArrayList;
import java.util.List;

/**
 * Driver class
 */
public class App {
    private static final String _usage = "Usage: ai.deeplens.App trial-csv-file patient-csv-file";

    public static void main( String[] args) {
        // Acceptance Criterion 1:
        //   Log each of the trials that is being processed and the number of patients that are being considered.
        //   Example: Processing trial id NCT03840902 for condition Non-small Cell Lung Cancer with 8 potential patients.
        //
        // Acceptance Criterion 2:
        //   Each patient must be evaluated for each trial. Log every time this evaluation is happening.
        //   Processing patient <patient_id>, age <age>, gender <gender>, with diagnosis <diagnosis>, for trial NCT03840902
        //
        // Acceptance Criterion 3:
        //   Check each patient against each of the criterion defined for the trial.
        //   **Age** - Ensure that the patient's age falls within the defined range of the trial (e.g. greater than 18)
        //   **Diagnosis** - Ensure that the patient's diagnosis matches one of the diagnoses defined on the trial.
        //   (note: the diagnosis list is a pipe delimited list within the csv). This match should be case insensitive.
        //   **Anatomic Site** - Ensure that the anatomic site provided for the patient's diagnosis matches what is
        //   defined within the trial. This match should be case insensitive.
        //
        // Acceptance Criterion 4:
        //   Patients are considered identified for a trial if they meet all of the required criteria.
        //   For criteria that are not required, a patient can still considered identified if their data matches or is
        //   not provided. If the data is available and does not match the criterion, the patient is not considered
        //   identified.
        //   **Age** - Not required
        //   **Diagnosis** - Required
        //   **Anatomic Site** - Required
        //
        // Acceptance Criterion 5:
        //   When all the patients are finished, log all of the patients that were identified for each trial.
        //   Example:
        //   The following 2 patients were identified for trial NCT03840902:
        //   Patient 1
        //   Patient 5

        if (args.length != 2) {
            System.err.println(_usage);
            System.exit(-1);
        }

        TrialReader trialReader = new TrialReaderCsvImpl(args[0]);
        PatientReader patientReader = new PatientReaderCsvImpl(args[1]);
        try {
            Trial trial = trialReader.read();
            List<Patient> patients = patientReader.read();
            logTrialInfo(trial, patients.size()); // Acceptance Criterion 1
            List<Patient> matchedPatients = new ArrayList<Patient>();
            Matcher matcher = MatcherImpl.getMatcher();
            for (Patient patient: patients) {
                logPatientEvaluation(patient, trial); // Acceptance Criterion 2
                if (matcher.match(trial, patient)) { // Acceptance Criteria 3 and 4
                    matchedPatients.add(patient);
                }
            }
            logMatchedPatients(matchedPatients, trial); // Acceptance Criterion 5
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Fulfill Acceptance Criterion 1:
     * Log each of the trials that is being processed and the number of patients that are being considered.
     * Example: Processing trial id NCT03840902 for condition Non-small Cell Lung Cancer with 8 potential patients.
     * @param trial
     * @param numPatients
     */
    private static void logTrialInfo(Trial trial, int numPatients) {
        System.out.println(
            "Processing trial id " + trial.getId() +
            " for " + trial.getTitle() +
            " with " + numPatients + " potential patients."
        );
    }

    /**
     * Fulfill Acceptance Criterion 2:
     * Each patient must be evaluated for each trial. Log every time this evaluation is happening.
     * Eg: Processing patient <patient_id>, age <age>, gender <gender>, with diagnosis <diagnosis>, for trial NCT03840902
     * @param patient
     * @param trial
     */
    private static void logPatientEvaluation(Patient patient, Trial trial) {
        System.out.println(
            "Processing patient " + patient.getId() +
            ", age " + patient.getAge() +
            ", gender " + patient.getGender() +
            ", with diagnosis " + patient.getDiagnosis() +
            ", for trial " + trial.getId()
        );

    }


    /**
     * Fulfill Acceptance Criterion 5:
     *   When all the patients are finished, log all of the patients that were identified for each trial.
     *   Example:
     *   The following 2 patients were identified for trial NCT03840902:
     *   Patient 1
     *   Patient 5
     * @param patients
     * @param trial
     */
    private static void logMatchedPatients(List<Patient> patients, Trial trial) {
        System.out.println(
            "The following " + patients.size() +
            " patients were identified for trial " + trial.getId()
        );
        for (Patient patient: patients) {
            System.out.println("Patient " + patient.getId());
        }
    }
}
