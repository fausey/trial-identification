package ai.deeplens;

/**
 * Defines getters for properties common to all implementations.
 */
public interface Patient {
    public String getId();
    public int getAge();
    public String getGender();
    public String getDiagnosis();
    public String getAnatomicSite();
}
