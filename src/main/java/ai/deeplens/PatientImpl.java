package ai.deeplens;

public class PatientImpl implements Patient {
    private String _id = "";
    private int _age = -1; // unknown
    private String _gender = "";
    private String _diagnosis = "";
    private String _anatomicSite = "";

    PatientImpl(String [] row) {
        if (row.length > 0) setId(row[0]);
        if (row.length > 1) {
            try {
                setAge(Integer.parseInt(row[1].trim()));
            } catch (NumberFormatException e) {
                setAge(-1);
            }
        }
        if (row.length > 2) setGender(row[2]);
        if (row.length > 3) setDiagnosis(row[3]);
        if (row.length > 4) setAnatomicSite(row[4]);
    }

    public void setId(String id) {
        if (id != null) _id = id.trim().toLowerCase();
        else _id = "";
    }

    public String getId() {
        return _id;
    }

    public void setAge(int age) {
        _age = age;
    }

    public int getAge() {
        return _age;
    }

    public void setGender(String gender) {
        if (gender != null) _gender = gender.trim().toLowerCase();
        else _gender = "";
    }

    public String getGender() {
        return _gender;
    }

    public void setDiagnosis(String diagnosis) {
        if (diagnosis != null) _diagnosis = diagnosis.trim().toLowerCase();
        else _diagnosis = "";
    }

    public String getDiagnosis() {
        return _diagnosis;
    }

    public void setAnatomicSite(String anatomicSite) {
        if (anatomicSite != null) _anatomicSite = anatomicSite.trim().toLowerCase();
        else _anatomicSite = "";
    }

    public String getAnatomicSite() {
        return _anatomicSite;
    }

    public String toString() {
        String s = "id: " + getId() + ", ";
        s += "age: " + getAge() + ", ";
        s += "gender: " + getGender() + ", ";
        s += "diagnosis: " + getDiagnosis() + ", ";
        s += "anatomic site: " + getAnatomicSite();
        return(s);
    }
}
