# Deep Lens Kata
## *Jon Fausey - 20220315*

### 0. Prerequisites (System Requirements)
The target system must have the following installed:

- Java 8
- Maven
- A git client

### 1. Clone the repo:
- Eg: `git clone https://fausey@bitbucket.org/fausey/trial-identification.git`

### 2. Build:
- Change directory into the directory into which you cloned the repo
- Run `mvn package` to compile, test and build a JAR file 

### 3. Run:
- Run sh src/main/scripts/match *trial-csv-file patient-csv-file*, eg:
  - `sh src/main/scripts/match src/test/resources/clinical_trial.csv src/test/resources/patient_feed.csv`

### Approach
My design is based on the assumption that there are likely to be numerous
variant file formats for the trial and patient input files depending on
who provides them.  Not just CSV files with differences, but perhaps also
JSON or XML files. 

In order to promote flexibility and ease of maintenance, I devised five
interfaces (which are admittedly overkill for this kata):

1. **Trial** - allows multiple implementation choices
    1. I wrote a simple implementation class - TrialImpl
2. **TrialReader** - may have an implementation for each trial file format
    1. I wrote an implementation for the trial CSV file format in the kata
    2. It would be relatively easy to write/inject new implementations for new file formats and would not introduce risk to this one
3. **Patient** - allows multiple implementation choices
    1. I wrote a simple implementation class - PatientImpl
4. **PatientReader** - may have an implementation for each patient file format
    1. I wrote an implementation for the patient CSV file format in the kata
    2. It would be relatively easy to write/inject new implementations for new file formats and would not introduce risk to this one
5. **Matcher** - may have an implementation for each unique set of trial rules for identifying patients
    1. I wrote an implementation based on the relevant rules (acceptance criteria) in the kata

The driver that pulls these concepts together is the class ai.deeplens.App.

I chose to write a shell script to make it as simple as possible to run it.