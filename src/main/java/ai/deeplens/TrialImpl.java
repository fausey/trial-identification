package ai.deeplens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * One possible implementation of the Trial interface.
 */
public class TrialImpl implements Trial {
    private String _id = "";
    private String _title = "";
    private String _description = "";
    private String _phase = "";
    private String _condition = "";
    private String _anatomicSite = "";
    private List<String> _diagnoses = null;
    private String _ageRequirement = "";

    public TrialImpl(String [] row) {
        if (row.length > 0) setId(row[0]);
        if (row.length > 1) setTitle(row[1]);
        if (row.length > 2) setDescription(row[2]);
        if (row.length > 3) setPhase(row[3]);
        if (row.length > 4) setCondition(row[4]);
        if (row.length > 5) setAnatomicSite(row[5]);
        if (row.length > 6) setDiagnoses(row[6]);
        if (row.length > 7) setAgeRequirement(row[7]);
    }

    public void setId(String id) {
        if (id != null) _id = id.trim();
        else _id = "";
    }

    public String getId() {
        return _id;
    }

    public void setTitle(String title) {
        if (title != null) _title = title.trim();
        else _title = "";
    }

    public String getTitle() {
        return _title;
    }

    public void setDescription(String description) {
        if (description != null) _description = description.trim();
        else _description = "";
    }

    public String getDescription() {
        return _description;
    }

    public void setPhase(String phase) {
        if (phase != null) _phase = phase.trim().toLowerCase();
        else _phase = "";
    }

    public String getPhase() {
        return _phase;
    }

    public void setCondition(String condition) {
        if (condition != null) _condition = condition.trim().toLowerCase();
        else _condition = "";
    }

    public String getCondition() {
        return _condition;
    }

    public void setAnatomicSite(String anatomicSite) {
        if (anatomicSite != null) _anatomicSite = anatomicSite.trim().toLowerCase();
        else _anatomicSite = "";
    }

    public String getAnatomicSite() {
        return _anatomicSite;
    }

    public void setDiagnoses(String diagnoses) {
        _diagnoses = new ArrayList<>();
        if (diagnoses != null) {
            for (String d: Arrays.asList(diagnoses.split("\\|"))) {
                _diagnoses.add(d.trim().toLowerCase());
            }
        }
    }

    public List<String> getDiagnoses() {
        return _diagnoses;
    }

    public void setAgeRequirement(String ageRequirement) {
        if (ageRequirement != null) _ageRequirement = ageRequirement.trim().toLowerCase();
        else _ageRequirement = "";
    }

    public String getAgeRequirement() {
        return _ageRequirement;
    }

    public String toString() {
        String s = "id: " + getId() + ", ";
        s += "title: " + getTitle() + ", ";
        s += "description: " + getDescription() + ", ";
        s += "phase: " + getPhase() + ", ";
        s += "condition: " + getCondition() + ", ";
        s += "anatomic site: " + getAnatomicSite() + ", ";
        s += "diagnoses: ";
        if (null != getDiagnoses()) {
            for (String d: getDiagnoses()) {
                s += d + "|";
            }
            s = s.substring(0,s.length()-1);
        } else {
            s += "null";
        }
        s += ", ";
        s += "age requirement: " + getAgeRequirement() + ".";
        return(s);
    }
}
