package ai.deeplens;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Assumes there may be multiple implementations (ie: multiple patient file
 * formats) and defines a common interface to be implemented.
 */
public interface PatientReader {
    List<Patient> read() throws IOException;
}
