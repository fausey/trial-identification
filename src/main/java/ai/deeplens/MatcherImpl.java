package ai.deeplens;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Arrays;

/**
 * Implements the Matcher interface given the rules of the kata.
 * Different rules could have a different implementation.
 */
public class MatcherImpl implements ai.deeplens.Matcher {
    private static ai.deeplens.Matcher _impl = new MatcherImpl(); // Singleton
    private MatcherImpl() {}
    public static ai.deeplens.Matcher getMatcher() {
        return _impl;
    }

    public boolean match(Trial trial, Patient patient) {
        //  Acceptance Criterion 3:
        //  Check each patient against each of the criterion defined for the trial.
        //  Age - Ensure that the patient's age falls within the defined range of the trial (e.g. greater than 18)
        //  Diagnosis - Ensure that the patient's diagnosis matches one of the diagnoses defined on the trial.
        //    (note: the diagnosis list is a pipe delimited list within the csv). This match should be case insensitive.
        //  Anatomic Site - Ensure that the anatomic site provided for the patient's diagnosis matches what is
        //    defined within the trial. This match should be case insensitive.

        //  Acceptance Criterion 4:
        //  Patients are considered identified for a trial if they meet all of the required criteria.
        //  For criteria that are not required, a patient can still considered identified if their data matches or is
        //  not provided. If the data is available and does not match the criterion, the patient is not considered
        //  identified.
        //  Age - Not required
        //  Diagnosis - Required
        //  Anatomic Site - Required

        if ((meetsAgeRequirement(trial, patient)) &&
            (diagnosisMatches(trial, patient)) &&
            (anatomicSiteMatches(trial, patient)))
                return true;

        return false;
    }


    public static boolean meetsAgeRequirement(Trial trial, Patient patient) {
        if (patient.getAge() < 0) return true; // patient age not provided

        String ageRequirement = trial.getAgeRequirement().trim();
        // TODO:  The specs for age range syntax are not provided.  Ask.
        // For now, I'm going to pretend that the following are acceptable:
        // >min, <max or min-max (inclusive range)
        try {
            if (ageRequirement.startsWith(">")) {
                int ageMin = Integer.parseInt(ageRequirement.substring(1,ageRequirement.trim().length()));
                return(patient.getAge() > ageMin);
            } else if (ageRequirement.startsWith("<")) {
                int ageMax = Integer.parseInt(ageRequirement.substring(1,ageRequirement.trim().length()));
                return(patient.getAge() < ageMax);
            } else {
                String regex = "^(\\d+)-(\\d+)$";
                Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
                Matcher matcher = pattern.matcher(ageRequirement);
                if (matcher.find()) {
                    int ageMin = Integer.parseInt(matcher.group(0));
                    int ageMax = Integer.parseInt(matcher.group(1));
                    return((patient.getAge() >= ageMin) && (patient.getAge() <= ageMax));
                } else {
                    throw new NumberFormatException();
                }
            }
        } catch (Exception e) {
                return false;
        }
    }

    public static boolean diagnosisMatches1(Trial trial, Patient patient) {
        // TODO:  Need to ask if trial diagnoses contains 'carcinoma' and patient diagnosis contains 'foo carcinoma'
        //  then is that a match?  If not, use this method.  If so, use the next method.
        return(trial.getDiagnoses().contains(patient.getDiagnosis()));
    }

    public static boolean diagnosisMatches(Trial trial, Patient patient) {
        String [] pWords = patient.getDiagnosis().split("\\s");
        for (String diagnosis: trial.getDiagnoses()) {
            String [] tWords = diagnosis.split("\\s");
            for (String tWord : tWords) {
                for (String pWord: pWords) {
                    if (pWord.equals(tWord)) return true;
                }
            }
        }
        return false;
    }

    public static boolean anatomicSiteMatches(Trial trial, Patient patient) {
        return(patient.getAnatomicSite().contains(trial.getAnatomicSite()));
    }
}

