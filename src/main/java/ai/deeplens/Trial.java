package ai.deeplens;

import java.util.List;

public interface Trial {
    public String getId();
    public String getTitle();
    public String getDescription();
    public String getPhase();
    public String getCondition();
    public String getAnatomicSite();
    public List<String> getDiagnoses();
    public String getAgeRequirement();
}
