package ai.deeplens;

import java.io.IOException;

/**
 * Assumes there may be multiple implementations (ie: multiple trial file
 * formats) and defines a common interface to be implemented.
 */
public interface TrialReader {
    public Trial read() throws IOException;
}
