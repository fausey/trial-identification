package ai.deeplens;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit test example.
 */
public class AppTest 
{
    /**
     * This is just an example. Better tests would be more granular and have more test cases.
     */
    @Test
    public void testMatch() {
        TrialReader trialReader = new TrialReaderCsvImpl("src/test/resources/clinical_trial.csv");
        PatientReader patientReader = new ai.deeplens.PatientReaderCsvImpl("src/test/resources/patient_feed.csv");
        try {
            ai.deeplens.Trial trial = trialReader.read();
            assertTrue(trial.getId().equals("NCT03840902"));
            List<ai.deeplens.Patient> patients = patientReader.read();
            assertTrue(patients.size() == 8);
            List<ai.deeplens.Patient> matchedPatients = new ArrayList<ai.deeplens.Patient>();
            ai.deeplens.Matcher matcher = ai.deeplens.MatcherImpl.getMatcher();
            for (ai.deeplens.Patient patient: patients) {
                if (matcher.match(trial, patient)) {
                    matchedPatients.add(patient);
                }
            }
            assertTrue(matchedPatients.size() == 2);
            assertTrue(matchedPatients.get(0).getId().equals("p-1"));
            assertTrue(matchedPatients.get(1).getId().equals("p-5"));

            // for (int i = 0; i < patients.size(); i++) {
            //     System.out.println("patients(" + i + ") = " + patients.get(i).getId());
            // }

            // Patient p-1
            assertTrue(MatcherImpl.meetsAgeRequirement(trial, patients.get(1)));
            assertTrue(MatcherImpl.diagnosisMatches(trial, patients.get(1)));
            assertTrue(MatcherImpl.anatomicSiteMatches(trial, patients.get(1)));

            // Patient p-3
            assertFalse(MatcherImpl.meetsAgeRequirement(trial, patients.get(3)));
            assertTrue(MatcherImpl.diagnosisMatches(trial, patients.get(3)));
            assertTrue(MatcherImpl.anatomicSiteMatches(trial, patients.get(3)));

            // Patient p-5
            assertTrue(MatcherImpl.meetsAgeRequirement(trial, patients.get(4)));
            assertTrue(MatcherImpl.diagnosisMatches(trial, patients.get(4)));
            assertTrue(MatcherImpl.anatomicSiteMatches(trial, patients.get(4)));

            // Patient p-8
            assertFalse(MatcherImpl.meetsAgeRequirement(trial, patients.get(7)));
            assertTrue(MatcherImpl.diagnosisMatches(trial, patients.get(7)));
            assertTrue(MatcherImpl.anatomicSiteMatches(trial, patients.get(7)));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
